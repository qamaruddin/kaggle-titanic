from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, LeakyReLU


def init_model():
    model = Sequential()
    model.add(Dense(70, input_dim=7))
    model.add(Dropout(0.2))
    model.add(LeakyReLU())
    model.add(Dense(50, input_dim=10))
    model.add(Dropout(0.1))
    model.add(LeakyReLU())
    model.add(Dense(1, input_dim=10))
    model.add(Activation('sigmoid'))
    return model
